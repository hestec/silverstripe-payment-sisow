<?php

class SisowProcessor extends PaymentProcessor {

    private static $allowed_actions = array(
        'capture',
        'complete'
    );

    public function capture($data) {

        parent::capture($data);

        // Set the return link
        $this->gateway->returnURL = Director::absoluteURL(Controller::join_links(
            $this->link(),
            'complete',
            $this->methodName,
            $this->payment->ID
        ));

        $this->gateway->method = $this->methodName;

        // Authorise the payment and get token
        $result = $this->gateway->authorise($this->paymentData);

        if ($result && !$result->isSuccess()) {
            $this->payment->updateStatus($result);
            $this->doRedirect();
            return;
        }

        // Save the TransactionID
        $this->payment->TransactionID = $this->gateway->trxId;
        $this->payment->write();

       // Process payment
        $result = $this->gateway->process($this->paymentData);

        // Processing may not get to here if all goes smoothly, customer will be at the 3rd party gateway
        if ($result && !$result->isSuccess()) {
            $this->payment->updateStatus($result);
            $this->doRedirect();
            return;
        }
    }

    public function complete($request) {

        // Reconstruct the payment object
        $this->payment = Payment::get()->byID($request->param('OtherID'));

        // Save or update the TransactionID
        $this->payment->TransactionID = $request->getVar('trxid');
        $this->payment->write();

        // Reconstruct the gateway object
        $methodName = $request->param('ID');
        $this->gateway = PaymentFactory::get_gateway($methodName);

        // Confirm the payment
        $data = array(
            'Status' => $request->getVar('status'),
            'Trxid' => $request->getVar('trxid'),
            'Ec' => $request->getVar('ec'),
            'Sha1' => $request->getVar('sha1'),
            'Amount' => $this->payment->Amount->Amount
        );

        $result = $this->gateway->confirm($data);
        $this->payment->updateStatus($result);

        // Do redirection
        $this->doRedirect();
        return;
    }

}
