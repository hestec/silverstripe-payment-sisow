<?php

class SisowGateway extends PaymentGateway_GatewayHosted {

    protected $supportedCurrencies = array(
        'EUR' => 'Euro',
    );


    public function authorise($data) {

        $response = '';

        $config = $this->getConfig();
        $merchantid = $config['authentication']['merchantid'];
        $merchantkey = $config['authentication']['merchantkey'];

        $sisow = new SisowRequest($merchantid, $merchantkey);

        switch($this->method){
            case 'Sisow_iDEAL':
                // for iDEAL payment is empty
                $sisow->payment = "";
                break;
            case 'Sisow_Overboeking':
                $sisow->payment = "overboeking";
                $sisow->billing_mail = $data['Email'];
                $sisow->billing_firstname = $data['Name'];
                $sisow->billing_lastname = "Hes";
                $sisow->including = "yes";
                $sisow->days = 7;
                break;
            case 'Sisow_Sofort':
                $sisow->payment = "sofort";
                break;
            case 'Sisow_MisterCash':
                $sisow->payment = "mistercash";
                break;
            case 'Sisow_PayPal':
                $sisow->payment = "paypalec";
                break;
            case 'Sisow_WebshopGiftcard':
                $sisow->payment = "webshop";
                break;
            case 'Sisow_PodiumCadeauKaart':
                $sisow->payment = "podium";
                break;
            case 'Sisow_Giropay':
                $sisow->payment = "giropay";
                break;
            case 'Sisow_MasterCard':
                $sisow->payment = "mastercard";
                break;
            case 'Sisow_VisaCard':
                $sisow->payment = "visa";
                break;
            case 'Sisow_Maestro':
                $sisow->payment = "maestro";
                break;
            case 'Sisow_EPS':
                $sisow->payment = "eps";
                break;
            case 'Sisow_INGHomePay':
                $sisow->payment = "homepay";
                break;
            case 'Sisow_VVV':
                $sisow->payment = "vvv";
                break;
            case 'Sisow_Focum':
                $sisow->payment = "focum";
                break;
            case 'Sisow_Klarna':
                $sisow->payment = "klarna";
                break;
        }

        $sisow->purchaseId = $data['Reference'];
        if (isset($data['Description'])){
            $sisow->description = substr($data['Description'], 0, 32); // max 32 characters, if empty the descrition in the Sisow panel will be used.
        }else{
            $sisow->description = $data['Reference'];
        }
        $sisow->amount = $data['Amount'];
        $sisow->issuerId = $data['IssuerID'];
        $sisow->returnUrl = $this->returnURL;

        // Sisow test mode true or false
        // first check if current ip address is in SisowPaymentTestIP
        $clientip = PaymentManagerTestIP::get()->filter('IpAddress', $_SERVER['REMOTE_ADDR'])->count();

        if (PaymentManagerConfig::get()->First()->TestMode == 1 && $clientip > 0){
            $sisow->testmode = "true";
        }else{
            $sisow->testmode = "false";
        }

        $sisow->TransactionRequest();
        $this->sisowErrorMessage = $sisow->errorMessage;
        $this->sisowErrorCode = $sisow->errorCode;

        if(!isset($sisow->issuerUrl) || !isset($sisow->trxId)){
            return new PaymentGateway_Failure($response, 'You are attempting to make a payment without the necessary credentials set');
        }
        else {
            $this->issuerUrl = $sisow->issuerUrl;
            $this->trxId = $sisow->trxId;
        }

    }

    public function process($data) {

        $paymentURL = $this->issuerUrl;

        if (!$paymentURL) {
            return new PaymentGateway_Failure(null, $this->sisowErrorCode." : ".$this->sisowErrorMessage);
        }

        Controller::curr()->redirect($paymentURL);

    }

    public function confirm($data) {

        $response = '';

        $config = $this->getConfig();
        $merchantid = $config['authentication']['merchantid'];
        $merchantkey = $config['authentication']['merchantkey'];

        $sisow = new SisowRequest($merchantid, $merchantkey);
        $sisow->trxId = $data['Trxid'];
        $sisow->StatusRequest();

        if(!isset($sisow->status) || $sisow->amount != $data['Amount']){
            $result = new PaymentGateway_Failure($response, 'You are attempting to make a payment without the necessary credentials set');
        }
        else {

            switch($sisow->status){
                case 'Success':
                    $result = new PaymentGateway_Success(
                        $response,
                        _t('SisowPayment.SUCCESS', 'The payment has been completed, and the funds have been successfully transferred')
                    );
                    break;
                case 'Expired':
                    $result = new PaymentGateway_Failure(
                        $response,
                        _t('SisowPayment.AUTHORISATION', 'The authorization period for this payment has been reached')
                    );
                    break;
                case 'Reversed':
                    $result = new PaymentGateway_Failure(
                        $response,
                        _t('SisowPayment.REVERSED', 'Payment was reversed')
                    );
                    break;
                case 'Failure':
                    $this->Status = 'Failure';
                    $result = new PaymentGateway_Failure(
                        $response,
                        _t('SisowPayment.FAILED', 'Payment failed')
                    );
                    break;
                case 'Pending':
                    $result = new PaymentGateway_Incomplete(
                        $response,
                        _t('SisowPayment.PENDING', 'The payment is pending')
                    );
                    break;
                case 'Cancelled':
                    $this->Status = 'Failure';
                    $result = new PaymentGateway_Failure(
                        $response,
                        _t('SisowPayment.FAILED', 'Payment cancelled')
                    );
                    break;

                default:
                    $result = new PaymentGateway_Incomplete(
                        $response,
                        _t('SisowPayment.DEFAULT', 'The payment is pending.')
                    );
                    break;
            }

        }

        return $result;
    }
}