# SilverStripe Payment Sisow #

### Overview ###

This module provides Sisow support for the SilverStripe Payment Module (https://github.com/silverstripe-labs/silverstripe-payment/tree/1.0). You need to be a merchant at Sisow (Dutch PSP), see for more information http://www.sisow.nl .

Sisow offers:

* iDEAL (The Netherlands)
* BanContact (Belgium, formerly named Mistercash)
* Sofort (international)
* PayPal (international)
* Giropay (Germany)
* Wire Transfer (international)
* Webshop Giftcard (The Netherlands)
* PodiumCadeauKaart (The Netherlands)
* MasterCard (international)
* Visa (international)
* Maestro (international)
* EPS (Austria)
* ING Home'Pay (Belgium)
* VVV Giftcard (The Netherlands)
* Focum Afterpay (international)
* Klarna Afterpay (international)

This module can handle all this methods.

If you use this module in combination with the SilverStripe Payment Manager (https://bitbucket.org/hestec/silverstripe-payment-manager) you can enable/disable the test modus to make test payments.

### Version ###

Using Semantic Versioning.

### Requirements ###

* SilverStripe > 3.1, tested untill 3.3.1.
* SilverStripe Payment Module 1.0.

### Installation ###

composer require hestec/silverstripe-payment-sisow 2.*

### Usage ###

Enable in your application YAML config (e.g: mysite/_config/payment.yaml):

```
PaymentGateway:
  environment:
    'dev'

PaymentProcessor:
  supported_methods:
    dev:
      - 'Sisow_iDEAL'
      - 'Sisow_MisterCash'
      - 'Sisow_Sofort'
      - 'Sisow_PayPal'
      - 'Sisow_Giropay'
      - 'Sisow_Overboeking'
      - 'Sisow_WebshopGiftcard'
      - 'Sisow_PodiumCadeauKaart'
      - 'Sisow_MasterCard'
      - 'Sisow_VisaCard'
      - 'Sisow_Maestro'
      - 'Sisow_EPS'
      - 'Sisow_INGHomePay'
      - 'Sisow_VVV'
      - 'Sisow_Focum'
      - 'Sisow_Klarna'

    live:
      - 'Sisow_iDEAL'
      - 'Sisow_MisterCash'
      - 'Sisow_Sofort'
      - 'Sisow_PayPal'
      - 'Sisow_Giropay'
      - 'Sisow_Overboeking'
      - 'Sisow_WebshopGiftcard'
      - 'Sisow_PodiumCadeauKaart'
      - 'Sisow_MasterCard'
      - 'Sisow_VisaCard'
      - 'Sisow_Maestro'
      - 'Sisow_EPS'
      - 'Sisow_INGHomePay'
      - 'Sisow_VVV'
      - 'Sisow_Focum'
      - 'Sisow_Klarna'

SisowGateway:
  live:
    authentication:
      merchantid: ''
      merchantkey: ''
  dev:
    authentication:
      merchantid: ''
      merchantkey: ''
      
### Todo/Issues ###

No known issues. For ideas or issues contact us.